#!/usr/bin/env bash

export KDEINSTALL=~/install
export PATH=$KDEINSTALL/bin:$PATH

export CMAKE_PREFIX_PATH=$KDEINSTALL:$CMAKE_PREFIX_PATH

export XDG_DATA_DIRS=$KDEINSTALL/share:$XDG_DATA_DIRS
export XDG_CONFIG_DIRS=$KDEINSTALL/etc/xdg

export QT_PLUGIN_PATH=$KDEINSTALL/lib/plugins
export QML2_IMPORT_PATH=$KDEINSTALL/lib/qml

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++

export MAKEFLAGS='-j4'

export ASAN_SYMBOLIZER_PATH=/usr/bin/llvm-symbolizer
# export ASAN_OPTIONS=symbolize=1

export KGAPI_SESSION_LOGFILE=/tmp/libkgapi

export KDE_FORK_SLAVES=1

# workaround for broken glibc/asan: https://llvm.org/bugs/show_bug.cgi?id=27310#c8
# export LD_PRELOAD=libasan.so:/home/elvis/fixasan.so

# export QT_MESSAGE_PATTERN="[%{pid} - %{category}] %{backtrace}: %{message}"
export QT_MESSAGE_PATTERN="[%{pid} - %{category}] %{function}(): %{message}"

# Disable noisy qproperty-without-notify clazy check.
export CLAZY_CHECKS="level0,level1,no-qproperty-without-notify"

alias fb='flatpak-builder --force-clean --ccache --require-changes --repo=repo app'
alias cmake='cmake -GNinja -DCMAKE_PREFIX_PATH="$KDEINSTALL" -DCMAKE_INSTALL_PREFIX="$KDEINSTALL" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DENABLE_CLAZY=ON'
if [ -v WAYLAND_DISPLAY ]; then
    alias ctest='ctest --output-on-failure'
else
    alias ctest='xvfb-run -s "-screen 0 1024x768x24" ctest --output-on-failure'
fi

# workaround for gnustep setting LD_LIBRARY_PATH
unset LD_LIBRARY_PATH
