if [ -f /etc/bash_completion ]; then
     . /etc/bash_completion
fi

if [ -f $HOME/.profile ]; then
    . $HOME/.profile
fi

# path valid at least on archlinux
if [ -f /usr/share/git/completion/git-prompt.sh ]; then
    . /usr/share/git/completion/git-prompt.sh
fi

# enable bash options
shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s expand_aliases
shopt -s extglob
shopt -s histappend
shopt -s hostcomplete
shopt -s nocaseglob

# used colors
No_Color='\e[0m'        # Color Reset
BCyan='\e[1;36m'        # Bold Cyan

# history stuff
export HISTSIZE=
export HISTFILESIZE=
export HISTCONTROL=ignoreboth
export HISTTIMEFORMAT="$(echo -e ${BCyan})[%d/%m %H:%M:%S]$(echo -e ${No_Color}) "

# define aliases
alias ls='ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias ll='ls -l --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias la='ls -lA --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias grep='grep --color=auto -d skip'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p'
alias df='df -kTh'                                                       # human-readable sizes and formatting
alias free='free -m'                                                     # show sizes in MB
alias ccat='pygmentize -g'                                               # requires pygments program
alias path='echo -e ${PATH//:/\\n}'                                      # pretty-print PATH variable
alias sml='rlwrap sml'                                                   # enable arrow keys (and command history) in sml-nj
alias open='xdg-open 2>> ~/.xsession-errors'

# ex - archive extractor
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xvjf $1   ;;
      *.tar.gz)    tar xvzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xvf $1    ;;
      *.tbz2)      tar xvjf $1   ;;
      *.tgz)       tar xvzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# enable arrow keys (and command history) in sml-nj interpreter
alias sml='rlwrap sml'

# prompt
PS1='\[\e[01;36m\][\u@\h\[\e[m\] \[\e[01;35m\]\w$(__git_ps1 " (%s)")\[\e[m\]\[\e[01;36m\]]\[\e[m\]\$ '

# fortune
if [ -x /usr/games/fortune ]; then
    /usr/games/fortune -s
fi

# Normal Colors
#Black='\e[0;30m'        # Black
#Red='\e[0;31m'          # Red
#Green='\e[0;32m'        # Green
#Yellow='\e[0;33m'       # Yellow
#Blue='\e[0;34m'         # Blue
#Purple='\e[0;35m'       # Purple
#Cyan='\e[0;36m'         # Cyan
#White='\e[0;37m'        # White

# Bold
#BBlack='\e[1;30m'       # Black
#BRed='\e[1;31m'         # Red
#BGreen='\e[1;32m'       # Green
#BYellow='\e[1;33m'      # Yellow
#BBlue='\e[1;34m'        # Blue
#BPurple='\e[1;35m'      # Purple
#BCyan='\e[1;36m'        # Cyan
#BWhite='\e[1;37m'       # White

# Background
#On_Black='\e[40m'       # Black
#On_Red='\e[41m'         # Red
#On_Green='\e[42m'       # Green
#On_Yellow='\e[43m'      # Yellow
#On_Blue='\e[44m'        # Blue
#On_Purple='\e[45m'      # Purple
#On_Cyan='\e[46m'        # Cyan
#On_White='\e[47m'       # White

#No_Color='\e[0m'        # Color Reset
