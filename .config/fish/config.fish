if status is-interactive
    # Customize $PATH
    fish_add_path ~/bin
    fish_add_path ~/.cargo/bin
    fish_add_path ~/.nimble/bin
    # Customize other env variables
    set -gx LANG "en_US.UTF-8"
    set -e LANGUAGE
    set -gx LESS "-Ri"
    set -gx EDITOR "vim"
    set -gx VISUAL "vim"
    set -gx PYTHONSTARTUP "/home/elvis/.pystartup"
    set -gx SSH_AUTH_SOCK $XDG_RUNTIME_DIR"/ssh-agent.socket"
    set -gx MANPAGER "sh -c 'col -bx | bat -l man -p'"
    set -gx MANROFFOPT "-c"
    # Disable greeting on fish start
    set -g fish_greeting
end
