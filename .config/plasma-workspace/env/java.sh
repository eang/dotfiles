# Fix fonts and force the GTK look-and-feel
export JAVA_TOOL_OPTIONS="-Dawt.useSystemAAFontSettings=on -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel"
