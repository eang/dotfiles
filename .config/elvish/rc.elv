use math
use path
use re
use str

# Optional paths, adds only those that exist
var optpaths = [
  ~/bin
  ~/.nimble/bin
]
var optpaths-filtered = [(each {|p|
      if (path:is-dir $p) { put $p }
} $optpaths)]

set paths = [
  $@optpaths-filtered
  /usr/bin
]

each {|p|
  if (not (path:is-dir &follow-symlink $p)) {
    echo (styled "Warning: directory "$p" in $paths no longer exists." red)
  }
} $paths

# Environment variables

set E:LESS = "-Ri"
set E:EDITOR = "vim"
set E:VISUAL = "vim"
set E:PYTHONSTARTUP = "/home/elvis/.pystartup"
set E:SSH_AUTH_SOCK = $E:XDG_RUNTIME_DIR"/ssh-agent.socket"
set E:MANPAGER = "sh -c 'col -bx | bat -l man -p'"

# Load starship prompt
eval (starship init elvish)
