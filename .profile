if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

PATH=$PATH:/usr/lib/smlnj/bin/:/home/elvis/phabricator/arcanist/bin/:.
PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"

export LESS="-Ri"
export EDITOR="vim"
export VISUAL="vim"
export LANG="en_US.UTF-8"
unset LANGUAGE

export PYTHONSTARTUP="~/.pystartup"

export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
